let form = document.getElementById("form");

form.addEventListener("submit", function(event) {
    event.preventDefault();

    let nama = document.getElementById("name").value;
    let password = document.getElementById("password").value;
    let email = document.getElementById("email").value;
    let date = document.getElementById("date").value;
    let telepon = document.getElementById("telepon").value;
    let gender = document.querySelector('input[name="gender"]:checked').value;

    const object = {
        nama: nama,
        password: password,
        email: email,
        date: date,
        telepon: telepon,
        gender: gender
    }

    localStorage.setItem("data", JSON.stringify(object));
    window.location.href="http://localhost/web-form-submission/home.html";
});

const getData = JSON.parse(localStorage.getItem("data"));

document.getElementById("nama").value = getData.nama;
document.getElementById("password").value = getData.password;
document.getElementById("email").value = getData.email;
document.getElementById("date").value = getData.date;
document.getElementById("telepon").value = getData.telepon;
document.getElementById("gender").value = getData.gender;